<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Test all cases</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>ab52fd89-9aa9-417f-9155-f6f175a01c87</testSuiteGuid>
   <testCaseLink>
      <guid>ceed6e05-5e8a-4a52-94e4-8534e9f0c9d3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login page test case</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3c29b2e1-3727-4211-abf2-eb31682bd0af</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Logout test case</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>d350e90c-434c-4545-b3c8-c2f7b11d7d53</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/User account</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>d350e90c-434c-4545-b3c8-c2f7b11d7d53</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>username</value>
         <variableId>c6d21a97-f68d-4fe9-a9cf-e70342593396</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>d350e90c-434c-4545-b3c8-c2f7b11d7d53</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>password</value>
         <variableId>8ecb1060-7347-482e-87e2-a08f9cba6c67</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>ebb94efb-138d-4164-bf90-d49ce3345419</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Empty username box test case</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d25c94d6-6a1d-4bbf-a197-990beb20fa17</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Empty password box test case</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>227188ad-8b07-4f8b-b412-f56ac2da40e8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login by Admin role test case</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e8a46834-c7ed-4c8c-8677-f1f1ac68f6db</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login by User role test case</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>9942a92b-e198-4d12-ad68-60213e4280b8</id>
         <iterationEntity>
            <iterationType>RANGE</iterationType>
            <value>1-2</value>
         </iterationEntity>
         <testDataId>Data Files/User account</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>9942a92b-e198-4d12-ad68-60213e4280b8</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>username</value>
         <variableId>a2132b4e-11b0-4926-a5ea-c4e2f2655b93</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>9942a92b-e198-4d12-ad68-60213e4280b8</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>password</value>
         <variableId>20b9a2a3-d7cf-4fb4-bfae-2a703974974d</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>d322ce74-a1fb-4d28-8f40-97c6ca772d35</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login by incorrect username and password test case</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>39e8c1c0-7fbf-450a-8dec-50f40aa9c9fd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Available products test case</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7b02e144-0c47-42fa-8db3-696b860463d7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Add to cart button test case</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2b258f7f-67c4-4fac-ab7b-aefecb50c582</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Select products and check cart update test case</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>1cbfd842-bf9f-44a7-9d42-3de390a4df5c</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/Select product</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>1cbfd842-bf9f-44a7-9d42-3de390a4df5c</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Amount 1</value>
         <variableId>5bf3dd47-f70f-40d3-a69f-ccab841448a8</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>1cbfd842-bf9f-44a7-9d42-3de390a4df5c</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Total 1</value>
         <variableId>aa6024ff-828c-4796-940c-d084bcba7eda</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>1cbfd842-bf9f-44a7-9d42-3de390a4df5c</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Amount 2</value>
         <variableId>2bda9d90-3cff-46b4-b46c-8b60db7b4eb6</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>1cbfd842-bf9f-44a7-9d42-3de390a4df5c</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Total 2</value>
         <variableId>cb5ea123-721d-4aa1-b7b8-e3946b91e5c4</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>1cbfd842-bf9f-44a7-9d42-3de390a4df5c</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Amount 3</value>
         <variableId>a009581b-163e-428c-a2b5-26062717a919</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>1cbfd842-bf9f-44a7-9d42-3de390a4df5c</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Total 3</value>
         <variableId>da765ada-2aa0-450b-b602-9d2c1c79ed84</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>1cbfd842-bf9f-44a7-9d42-3de390a4df5c</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Amount 4</value>
         <variableId>d7a18fe0-9645-4c86-935d-ef9e6fdf0092</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>1cbfd842-bf9f-44a7-9d42-3de390a4df5c</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Total 4</value>
         <variableId>2dec8c2d-c67d-4291-8b99-4884f934db41</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>1cbfd842-bf9f-44a7-9d42-3de390a4df5c</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Amount 5</value>
         <variableId>0f46e35b-6960-4dd2-87d0-3d9ceaa4186f</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>1cbfd842-bf9f-44a7-9d42-3de390a4df5c</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Total 5</value>
         <variableId>fa346c7c-a31e-4dc2-a9a9-636e57f467b6</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>fc740bfb-eea2-46af-b321-f74be658898e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Total transaction shown for admin test case</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
