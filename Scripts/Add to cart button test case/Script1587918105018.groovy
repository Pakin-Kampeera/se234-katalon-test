import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('http://3.88.252.221:8081/')

WebUI.setText(findTestObject('Page_ProjectBackend/input_Username_username'), 'user')

WebUI.setEncryptedText(findTestObject('Page_ProjectBackend/input_Password_password'), '1/VWEm4uipk=')

WebUI.click(findTestObject('Page_ProjectBackend/button_Login'))

WebUI.verifyElementText(findTestObject('Page_ProjectBackend/a_Products'), 'Products')

WebUI.click(findTestObject('Page_ProjectBackend/button_add to cart 1'))

WebUI.verifyElementText(findTestObject('Page_ProjectBackend/button_add to cart 1'), 'already added')

WebUI.verifyElementText(findTestObject('Page_ProjectBackend/span'), '1')

WebUI.click(findTestObject('Page_ProjectBackend/button_add to cart 2'))

WebUI.verifyElementText(findTestObject('Page_ProjectBackend/button_add to cart 2'), 'already added')

WebUI.verifyElementText(findTestObject('Page_ProjectBackend/span'), '2')

WebUI.click(findTestObject('Page_ProjectBackend/button_add to cart 3'))

WebUI.verifyElementText(findTestObject('Page_ProjectBackend/button_add to cart 3'), 'already added')

WebUI.verifyElementText(findTestObject('Page_ProjectBackend/span'), '3')

WebUI.click(findTestObject('Page_ProjectBackend/button_add to cart 4'))

WebUI.verifyElementText(findTestObject('Page_ProjectBackend/button_add to cart 4'), 'already added')

WebUI.verifyElementText(findTestObject('Page_ProjectBackend/span'), '4')

WebUI.click(findTestObject('Page_ProjectBackend/button_add to cart 5'))

WebUI.verifyElementText(findTestObject('Page_ProjectBackend/button_add to cart 5'), 'already added')

WebUI.verifyElementText(findTestObject('Page_ProjectBackend/span'), '5')

WebUI.closeBrowser()

